package cat.itb.diceroll;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ImageView titleImage;
    Button rollButton;
    ImageView result1;
    ImageView result2;
    Button restartAppearence;
    boolean rolled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rollButton = findViewById(R.id.rollButton);
        titleImage = findViewById(R.id.titleImage);
        result1 = findViewById(R.id.resultImage1);
        result2 = findViewById(R.id.resultImage2);
        restartAppearence = findViewById(R.id.restartAppearence);

        final int[] dice = new int[6];
        dice[0] = R.drawable.dice_1;
        dice[1] = R.drawable.dice_2;
        dice[2] = R.drawable.dice_3;
        dice[3] = R.drawable.dice_4;
        dice[4] = R.drawable.dice_5;
        dice[5] = R.drawable.dice_6;


        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rolled = true;
                //Toast.makeText(MainActivity.this, "Button Clicked", Toast.LENGTH_LONG).show();
                Random r = new Random();
                int resultNumber1 = r.nextInt(6);
                result1.setImageResource(dice[resultNumber1]);
                int resultNumber2 = r.nextInt(6);
                result2.setImageResource(dice[resultNumber2]);

                if (resultNumber1 == 5 && resultNumber2 == 5) {
                    Toast toast= Toast.makeText(getApplicationContext(),
                            "JACKPOT!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP,0,0);
                    toast.show();
                }
            }
        });

        restartAppearence.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                result1.setImageResource(R.drawable.empty_dice);
                result2.setImageResource(R.drawable.empty_dice);
                Toast.makeText(MainActivity.this, "Appearence Restarted.", Toast.LENGTH_LONG).show();
            }
        });

        result1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rolled) {
                    Random r = new Random();
                    int resultNumber1 = r.nextInt(6);
                    result1.setImageResource(dice[resultNumber1]);

                } else {
                    Toast.makeText(MainActivity.this, "Haven't done any rolls yet!.", Toast.LENGTH_LONG).show();
                }
            }
        });


        result2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rolled) {
                    Random r = new Random();
                    int resultNumber2 = r.nextInt(6);
                    result2.setImageResource(dice[resultNumber2]);
                } else {
                    Toast.makeText(MainActivity.this, "Haven't done any rolls yet!.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

}
